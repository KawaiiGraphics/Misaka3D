#include "OrphanShader.hpp"
#include "MisakaFunctions.hpp"

OrphanShader::OrphanShader(GLuint id):
  id(id)
{ }

void OrphanShader::destroy(MisakaFunctions &gl)
{
  gl.glDeleteShader(id);
}
