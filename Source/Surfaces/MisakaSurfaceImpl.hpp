#ifndef MISAKASURFACE_HPP
#define MISAKASURFACE_HPP

#include "Misaka3D_global.hpp"

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include <Kawaii3D/Surfaces/KawaiiSurface.hpp>

#include <QOpenGLPaintDevice>
#include <memory>

class MISAKA3D_SHARED_EXPORT MisakaSurfaceImpl: public KawaiiRendererImpl
{
  Q_OBJECT

public:
  MisakaSurfaceImpl(KawaiiSurface *model);
  ~MisakaSurfaceImpl();



  //IMPLEMENT
private:
  QPointer<KawaiiSurface> model;
  std::unique_ptr<QOpenGLPaintDevice> glDev;

  void render();
  void onModelResized(const QSize &sz);
};

#endif // MISAKASURFACE_HPP
