#include "MisakaTextureWrapMode.hpp"

MisakaTextureWrapMode getMisakaTextureWrapMode(KawaiiTextureWrapMode value)
{
  switch(value)
    {
    case KawaiiTextureWrapMode::ClampToEdge:
      return MisakaTextureWrapMode::ClampToEdge;

    case KawaiiTextureWrapMode::ClampToBorder:
      return MisakaTextureWrapMode::ClampToBorder;

    case KawaiiTextureWrapMode::MirroredRepeat:
      return MisakaTextureWrapMode::MirroredRepeat;

    case KawaiiTextureWrapMode::Repeat:
      return MisakaTextureWrapMode::Repeat;

    case KawaiiTextureWrapMode::MirroredClampToEdge:
      return MisakaTextureWrapMode::MirroredClampToEdge;
    }

  return MisakaTextureWrapMode::Repeat;
}
