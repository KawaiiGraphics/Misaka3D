#include <Kawaii3D/Geometry/KawaiiMeshInstance.hpp>
#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>
#include "MisakaMeshInstanceImpl.hpp"
#include "Geometry/MisakaVao3D.hpp"
#include <Kawaii3D/KawaiiRoot.hpp>
#include "MisakaMeshImpl.hpp"
#include "MisakaRootImpl.hpp"
#include <memory>

MisakaMeshInstanceImpl::MisakaMeshInstanceImpl(KawaiiMeshInstance *model):
  KawaiiRendererImpl(model),
  model(model),
  vao(nullptr)
{
  if(model)
    connect(model, &KawaiiMeshInstance::meshChanged, this, &MisakaMeshInstanceImpl::onMeshChanged);
  else
    deleteLater();
}

MisakaMeshInstanceImpl::~MisakaMeshInstanceImpl()
{
}

void MisakaMeshInstanceImpl::draw()
{
  if(!vao)
    return;

  if(auto ubo = getUniforms(); ubo)
    ubo->bind(KawaiiShader::getModelUboLocation());

  switch (model->getInstanceCount())
    {
    case 0:
      break;

    case 1:
      vao->draw();
      break;

    default:
      vao->drawInstanced(model->getInstanceCount());
      break;
    }
}

MisakaGpuBufImpl *MisakaMeshInstanceImpl::getUniforms() const
{
  if(model->getUniforms())
    return static_cast<MisakaGpuBufImpl*>(model->getUniforms()->getRendererImpl());
  else
    return nullptr;
}

void MisakaMeshInstanceImpl::setVao(MisakaVao3D *vao)
{
  this->vao = vao;
}

void MisakaMeshInstanceImpl::onMeshChanged()
{
  if(!model->getMesh()) return;

  auto mesh = static_cast<MisakaMeshImpl*>(model->getMesh()->getRendererImpl());
  if(mesh)
    mesh->addChildInstance(this);
}

void MisakaMeshInstanceImpl::initConnections()
{
  onMeshChanged();
}
