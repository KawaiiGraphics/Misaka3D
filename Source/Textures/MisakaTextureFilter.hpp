#ifndef MISAKATEXTUREFILTER_HPP
#define MISAKATEXTUREFILTER_HPP

#include <qopengl.h>
#include "Misaka3D_global.hpp"
#include <Kawaii3D/Textures/KawaiiTextureFilter.hpp>

enum class MisakaTextureFilter: GLenum
{
  Linear                = GL_LINEAR,
  Nearest               = GL_NEAREST,
  NearestMipmapNearest  = GL_NEAREST_MIPMAP_NEAREST,
  LinearMipmapNearest   = GL_LINEAR_MIPMAP_NEAREST,
  NearestMipmapLinear   = GL_NEAREST_MIPMAP_LINEAR,
  LinearMipmapLinear    = GL_LINEAR_MIPMAP_LINEAR
};

MISAKA3D_SHARED_EXPORT MisakaTextureFilter getMisakaTextureFilter(KawaiiTextureFilter value);
MISAKA3D_SHARED_EXPORT bool needsMipMap(MisakaTextureFilter filter);

#endif // MISAKATEXTUREFILTER_HPP
