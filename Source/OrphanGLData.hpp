#ifndef ORPHANGLDATA_HPP
#define ORPHANGLDATA_HPP

class MisakaFunctions;

class OrphanGLData {
public:
  virtual ~OrphanGLData() = default;
  virtual void destroy(MisakaFunctions &gl) = 0;

  inline static void destroyData(OrphanGLData *d, MisakaFunctions &gl)
  { if(!d) return; d->destroy(gl); delete d; }
};

#endif // ORPHANGLDATA_HPP
