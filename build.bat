mkdir pkg
mkdir tmp
cd tmp
cmake -G "Visual Studio 15 2017 Win64" -DCMAKE_INSTALL_PREFIX="../pkg" -DCMAKE_BUILD_TYPE=Release ../
cmake --build . --config Release
cmake --build . --config Release --target install
cd ..