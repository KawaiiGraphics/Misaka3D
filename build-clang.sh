#!/bin/sh
export CC=/usr/bin/clang
export CXX=/usr/bin/clang++
: ${CMAKE_INSTALL_PREFIX:='/usr/local'}
SCRIPT=$(readlink -f "$0")
SRC_DIR=$(dirname "$SCRIPT")
cmake -DCMAKE_INSTALL_PREFIX="${CMAKE_INSTALL_PREFIX}" -DCMAKE_BUILD_TYPE=Release "$SRC_DIR"
cmake --build . --config Release
