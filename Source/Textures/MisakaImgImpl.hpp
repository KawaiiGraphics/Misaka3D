#ifndef MISAKAIMGIMPL_HPP
#define MISAKAIMGIMPL_HPP

#include "MisakaTextureImpl.hpp"
#include "MisakaImg.hpp"
#include <Kawaii3D/Textures/KawaiiImage.hpp>

class MisakaRootImpl;

class MISAKA3D_SHARED_EXPORT MisakaImgImpl : public MisakaTextureImpl
{
  Q_OBJECT

public:
  explicit MisakaImgImpl(KawaiiImage *model);
  ~MisakaImgImpl();



  //IMPLEMENT
private:
  KawaiiImage *model;
  MisakaRootImpl *root;

  void createView();
  void updateImg();
};

#endif // MISAKAIMGIMPL_HPP
