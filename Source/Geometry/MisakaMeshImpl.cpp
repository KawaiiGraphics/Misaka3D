#include "MisakaMeshImpl.hpp"
#include "MisakaVao3D.hpp"
#include "MisakaRootImpl.hpp"
#include "MisakaMeshInstanceImpl.hpp"
#include <Kawaii3D/KawaiiRoot.hpp>
#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>
#include <QChildEvent>

MisakaMeshImpl::MisakaMeshImpl(KawaiiMesh3D *model):
  KawaiiRendererImpl(model),
  vao(nullptr),
  model(model),
  root(nullptr)
{
  if(model)
    {
      connect(model, &KawaiiMesh3D::vertexAdded,     this, &MisakaMeshImpl::vertexAdded);
      connect(model, &KawaiiMesh3D::vertexRemoved,   this, &MisakaMeshImpl::vertexRemoved);
      connect(model, &KawaiiMesh3D::vertexUpdated,   this, &MisakaMeshImpl::vertexUpdated);
      connect(model, &KawaiiMesh3D::triangleAdded,   this, &MisakaMeshImpl::trianglesRelocated);
      connect(model, &KawaiiMesh3D::triangleRemoved, this, &MisakaMeshImpl::trianglesRelocated);
      connect(model, &KawaiiMesh3D::indexUpdated,    this, &MisakaMeshImpl::indexUpdated);
      connect(model, &KawaiiMesh3D::parentUpdated,   this, &MisakaMeshImpl::onDataParentChanges);
      onDataParentChanges();
    }
}

MisakaMeshImpl::~MisakaMeshImpl()
{
  if(vao)
    {
      auto d = vao.data();
      vao.clear();
      delete d;
    }
}

void MisakaMeshImpl::addChildInstance(MisakaMeshInstanceImpl *inst)
{
  if(inst)
    inst->setVao(vao);
}

void MisakaMeshImpl::createView()
{
  if(root)
    {
      vao = new MisakaVao3D(root->getContext());
      vao->setIndices(model->getRawTriangles());

      baseAttrCtrl.setBuffer(&vao->getBaseAttrBuf());
      baseAttrCtrl.setData(model->getVertices());
    }
}

void MisakaMeshImpl::vertexAdded()
{
  baseAttrCtrl.setData(model->getVertices());
}

void MisakaMeshImpl::vertexRemoved()
{
  baseAttrCtrl.setData(model->getVertices());
}

void MisakaMeshImpl::vertexUpdated(int index)
{
  baseAttrCtrl.updateDataElements<KawaiiPoint3D>(index, 1);
}

void MisakaMeshImpl::trianglesRelocated()
{
  vao->setIndices(model->getRawTriangles());
}

void MisakaMeshImpl::indexUpdated(int i)
{
  vao->updateIndex(i);
}

void MisakaMeshImpl::onDataParentChanges()
{
  auto _root = MisakaRootImpl::findRootImpl(this);
  if(_root != root)
    {
      if(vao)
        {
          vao->deleteLater();
          vao.clear();
        }
      root = _root;
      createView();
    }
}
