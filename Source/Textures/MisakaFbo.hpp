#ifndef MISAKAFBO_HPP
#define MISAKAFBO_HPP

#include "MisakaTexture.hpp"

class MisakaRenderbuffer;
class MISAKA3D_SHARED_EXPORT MisakaFbo: public MisakaGLObject
{
  Q_OBJECT

public:
  template<typename... ArgsT>
  MisakaFbo(ArgsT&&... args):
    MisakaGLObject(std::forward<ArgsT>(args)...),
    clearMask(0)
  {}

  ~MisakaFbo() = default;

  void bind() const;
  void unbind(GLuint defaultFramebuffer = 0);

  void attach(MisakaTexture *texture, int texLayer, GLenum mode);
  void attach(MisakaRenderbuffer *renderbuffer, GLenum mode);

  void prepareRendering();
  void doneRendering();

  // MisakaGLObject interface
private:
  void initialize(GLuint &id) override;
  inline void updateNow() override {}



  //IMPLEMENT
private:
  GLenum clearMask;

  void attach(GLenum mode);
};

#endif // MISAKAFBO_HPP
