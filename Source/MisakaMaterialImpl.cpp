#include "MisakaMaterialImpl.hpp"
#include "MisakaGLBuffer.hpp"
#include "MisakaRootImpl.hpp"

#include <Kawaii3D/KawaiiGpuBuf.hpp>
#include <Kawaii3D/KawaiiMaterial.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>

MisakaMaterialImpl::MisakaMaterialImpl(KawaiiMaterial *model):
  KawaiiRendererImpl(model),
  model(model),
  ubo(nullptr)
{
  if(!model)
    deleteLater();
}

MisakaMaterialImpl::~MisakaMaterialImpl()
{
}

void MisakaMaterialImpl::bind() const
{
  auto ubo = model->getUniforms()?
        static_cast<MisakaGpuBufImpl*>(model->getUniforms()->getRendererImpl()):
        nullptr;

  if(ubo)
    ubo->bind(KawaiiShader::getMaterialUboLocation());
}
