#ifndef MISAKABUFFERIMPL_HPP
#define MISAKABUFFERIMPL_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include "MisakaGLBuffer.hpp"
#include <QPointer>

class MISAKA3D_SHARED_EXPORT MisakaBufferImpl: public KawaiiRendererImpl
{
  Q_OBJECT

public:
  explicit MisakaBufferImpl(MisakaGLBuffer *glBuf = nullptr);

  template<typename... ArgsT>
  MisakaBufferImpl(MisakaGLBuffer *glBuf, ArgsT&&... args):
    KawaiiRendererImpl(std::forward<ArgsT>(args)...),
    glBuf(glBuf)
  { }

  void setBuffer(MisakaGLBuffer *buf);
  void deleteBuffer();

  inline auto getBuffer() const
  {
    return glBuf.data();
  }

  template<typename T>
  inline void setData(const QVector<T> &vec)
  { glBuf->setData(vec.data(), sizeof(T)*vec.size()); }

  template<typename T>
  inline void updateDataElements(size_t first, size_t count)
  {
    glBuf->updateData(reinterpret_cast<const void*>(reinterpret_cast<size_t>(glBuf->data) + first * sizeof(T)),
                      count * sizeof(T));
  }

  template<typename T>
  inline size_t getElemetsCount() const
  {
    return glBuf->getDataSize() / sizeof(T);
  }

  ~MisakaBufferImpl() = default;



  //IMPLEMENT
private:
  QPointer<MisakaGLBuffer> glBuf;
};

#endif // MISAKABUFFERIMPL_HPP
