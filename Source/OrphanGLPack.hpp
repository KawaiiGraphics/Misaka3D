#ifndef ORPHANGLPACK_HPP
#define ORPHANGLPACK_HPP

#include <type_traits>
#include <tuple>
#include "OrphanGLData.hpp"

template<typename... ArgsT>
class OrphanGLPack: public OrphanGLData
{
  std::tuple<ArgsT...> data;

  template<int i, class = typename std::enable_if<(i >= 0)>::type >
  void destroyHelper(MisakaFunctions &gl)
  {
    std::get<i>(data).destroy(gl);
    destroyHelper<i-1>(gl);
  }

  template<int i>
  inline void destroyHelper(MisakaFunctions &, typename std::enable_if<(i < 0)>::type* = nullptr)
  { }

public:
  OrphanGLPack(ArgsT&&... args):
    data(std::make_tuple(std::forward<ArgsT>(args)...))
  {}
  ~OrphanGLPack() = default;

  void destroy(MisakaFunctions &gl) override final { destroyHelper<std::tuple_size<std::tuple<ArgsT...>>::value - 1>(gl); }
};

#endif // ORPHANGLPACK_HPP
