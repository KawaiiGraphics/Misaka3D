#include "MisakaDirtyGLObjects.hpp"
#include "MisakaGLObject.hpp"

void MisakaDirtyGLObjects::check()
{
  while(!lst.isEmpty())
    lst.takeFirst()->check();
}

void MisakaDirtyGLObjects::update(MisakaGLObject *obj)
{
  if(!lst.contains(obj))
    lst.push_back(obj);
}

void MisakaDirtyGLObjects::remove(MisakaGLObject *obj)
{
  lst.removeOne(obj);
}
