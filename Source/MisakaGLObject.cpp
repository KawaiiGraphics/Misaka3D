#include "MisakaGLObject.hpp"
#include "MisakaContext3D.hpp"

MisakaGLObject::MisakaGLObject(MisakaContext3D *ctx) : QObject(ctx),
  glParent(nullptr), ctx(ctx), orphanHandle(nullptr), id(0), exists(false)
{
  if(ctx) {
      ctx->glObjects.push_back(this);
      update();
    }
}

MisakaGLObject::MisakaGLObject(MisakaContext3D *ctx, QObject *parent) : QObject(parent),
  glParent(nullptr), ctx(ctx), orphanHandle(nullptr), id(0), exists(false)
{
  if(ctx) {
      ctx->glObjects.push_back(this);
      update();
    }
}

MisakaGLObject::MisakaGLObject(MisakaGLObject *glParent, QObject *parent): QObject(parent),
  glParent(glParent), ctx(glParent->ctx), orphanHandle(nullptr), id(0), exists(false)
{
  glParent->glChildren.push_back(this);
  update();
}

MisakaGLObject::~MisakaGLObject()
{
  if(ctx)
    {
      if(glParent)
        {
          glParent->glChildren.removeOne(this);
          glParent->dirtyChildren.remove(this);
        } else {
          ctx->glObjects.removeOne(this);
          ctx->dirtyObjects.remove(this);
        }

      if(orphanHandle)
        ctx->orphan.push_back(orphanHandle);
    }
  for(auto &i: glChildren)
    i->glParent = nullptr;
}

void MisakaGLObject::update()
{
  if(glParent)
    glParent->dirtyChildren.update(this);
  else
    if(ctx)
      ctx->dirtyObjects.update(this);
}

void MisakaGLObject::destroy()
{
  auto& GL = gl();
  ctx = nullptr;
  OrphanGLData::destroyData(orphanHandle, GL);
}

void MisakaGLObject::checkDirtyChildren()
{
  dirtyChildren.check();
}

void MisakaGLObject::check()
{
  checkDirtyChildren();
  if(!exists) {
      id = 0;
      initialize(id);
      exists = true;

      emit initialized();
    }
  updateNow();
}

void MisakaGLObject::setOrphanHandle(OrphanGLData *handle)
{
  if(orphanHandle)
    delete orphanHandle;
  orphanHandle = handle;
}

OrphanGLData *MisakaGLObject::getOrphanHandle() const
{
  return orphanHandle;
}

void MisakaGLObject::reinit()
{
  if(exists)
    {
      if(orphanHandle)
        {
          ctx->orphan.push_back(orphanHandle);
          orphanHandle = nullptr;
        }
      exists = false;
      update();
    }
}
