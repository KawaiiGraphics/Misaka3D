#include <Kawaii3D/KawaiiRoot.hpp>
#include "MisakaSurfaceImpl.hpp"
#include "MisakaCameraImpl.hpp"
#include "MisakaContext3D.hpp"
#include "MisakaRootImpl.hpp"


MisakaSurfaceImpl::MisakaSurfaceImpl(KawaiiSurface *model):
  KawaiiRendererImpl(model),
  model(model),
  glDev(nullptr)
{
  if(model)
    {
      model->setRenderFunc(std::bind(&MisakaSurfaceImpl::render, this));
      connect(model, &KawaiiSurface::resized, this, &MisakaSurfaceImpl::onModelResized);
    }
  else
    deleteLater();
}

MisakaSurfaceImpl::~MisakaSurfaceImpl()
{
  if(model)
    model->detachRenderFunc();
}

void MisakaSurfaceImpl::render()
{
  if(!model->getRoot())
    return;

  auto root = static_cast<MisakaRootImpl*>(model->getRoot()->getRendererImpl());
  if(!root)
    return;

  auto *ctx = root->getContext();
  ctx->beginDraw(&model->getWindow());
  for(size_t i = 0; i < model->tileCount(); ++i)
    {
      auto &tile = model->getTile(i);
      if(tile.getCamera())
        {
          ctx->setViewport(tile.getResultRect());
          auto cam = static_cast<MisakaCameraImpl*>(tile.getCamera()->getRendererImpl());
          auto bufImpl = static_cast<MisakaGpuBufImpl*>(tile.getSurfaceGpuVar().getRendererImpl());
          if(cam && bufImpl)
            cam->draw(bufImpl->getBuffer());
        }
    }

  if(model->hasOverlays())
    {
      if(!glDev)
        glDev = std::make_unique<QOpenGLPaintDevice>(model->getSize());

      model->drawOverlays(glDev.get());
      glEnable(GL_DEPTH_TEST);
    }
  ctx->endDraw(&model->getWindow());
}

void MisakaSurfaceImpl::onModelResized(const QSize &sz)
{
  if(glDev)
    glDev->setSize(sz);
}
