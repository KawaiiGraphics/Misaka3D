#ifndef MISAKAROOTIMPL_HPP
#define MISAKAROOTIMPL_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>
#include "Misaka3D_global.hpp"
#include <qopengl.h>

class MisakaContext3D;
class KawaiiShader;

class MISAKA3D_SHARED_EXPORT MisakaRootImpl : public KawaiiRendererImpl
{
  Q_OBJECT

public:
  explicit MisakaRootImpl(KawaiiRoot *model);
  virtual ~MisakaRootImpl();

  inline auto* getContext() const { return ctx; }

  static MisakaRootImpl* findRootImpl(KawaiiRendererImpl *rnd);

signals:
  void compiledShader(KawaiiShader *shader, GLuint glId);
  void removedShader(KawaiiShader *shader, GLuint glId);



  //IMPLEMENT
private:
  MisakaContext3D *ctx;
};

#endif // MISAKAROOTIMPL_HPP
