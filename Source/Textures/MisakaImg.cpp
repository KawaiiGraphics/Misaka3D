#include "../MisakaFunctions.hpp"
#include "MisakaImg.hpp"
#include <glm/vec2.hpp>
#include <QImage>
#include <memory>

MisakaImg::MisakaImg(MisakaContext3D *ctx, const QImage *img):
  MisakaTexture(ctx,
                glm::uvec4(img->width(), img->height(), 0, 0),
                MisakaTextureType::Texture2D),
  img(img)
{
}

namespace {
  GLenum getImgFmt(const QImage *img, std::unique_ptr<QImage> &imgPtr)
  {
    if(imgPtr && img != imgPtr.get())
      return getImgFmt(imgPtr.get(), imgPtr);

    switch(img->format())
      {
      case QImage::Format_RGB32:
      case QImage::Format_ARGB32:
        return GL_BGRA;

      case QImage::Format_RGBA8888:
        return GL_RGBA;

      default:
        imgPtr.reset(new QImage(img->convertToFormat(QImage::Format_ARGB32)));
        return GL_BGRA;
      }
  }
}

void MisakaImg::fill()
{
  auto fmt = getImgFmt(img, convertedImg);
  gl().glTextureSubImage2D(ID(), 0, 0, 0, img->width(), img->height(), fmt, MisakaGLVariableType::UByte, constBits());
}

const uchar *MisakaImg::constBits() const
{
  return convertedImg? convertedImg->constBits(): img->constBits();
}
