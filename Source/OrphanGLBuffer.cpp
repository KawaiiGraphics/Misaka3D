#include "OrphanGLBuffer.hpp"
#include "MisakaFunctions.hpp"

OrphanGLBuffer::OrphanGLBuffer(GLuint id):
  id(id)
{ }

void OrphanGLBuffer::destroy(MisakaFunctions &gl)
{
  gl.glDeleteBuffers(1, &id);
}
