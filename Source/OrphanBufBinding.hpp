#ifndef ORPHANBUFBINDING_HPP
#define ORPHANBUFBINDING_HPP

#include "OrphanGLData.hpp"
#include "Misaka3D_global.hpp"
#include "MisakaGLBufferType.hpp"
#include <qopengl.h>
#include <QObject>

class MisakaGpuBufImpl;

class MISAKA3D_SHARED_EXPORT OrphanBufBinding : public OrphanGLData
{
  QMetaObject::Connection onTargetDestroyed;
  MisakaGpuBufImpl *buffer;
  GLuint bindingPoint;
  MisakaGLBufferType target;
public:
  OrphanBufBinding(MisakaGpuBufImpl *buffer, GLuint bindingPoint, MisakaGLBufferType target);
  void destroy(MisakaFunctions &gl) override final;
};

#endif // ORPHANBUFBINDING_HPP
