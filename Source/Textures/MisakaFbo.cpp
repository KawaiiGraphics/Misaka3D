#include "MisakaFbo.hpp"
#include "OrphanFbo.hpp"
#include "MisakaRenderbuffer.hpp"

#include <QDebug>
#include <Kawaii3D/KawaiiConfig.hpp>

void MisakaFbo::initialize(GLuint &id)
{
  gl().glCreateFramebuffers(1, &id);
  setOrphanHandle(new OrphanFbo(id));
}

void MisakaFbo::attach(GLenum mode)
{
  if(mode >= GL_COLOR_ATTACHMENT0 && mode <= GL_COLOR_ATTACHMENT7)
    clearMask |= GL_COLOR_BUFFER_BIT;
  else
    if(mode == GL_DEPTH_ATTACHMENT)
      clearMask |= GL_DEPTH_BUFFER_BIT;
}

void MisakaFbo::prepareRendering()
{
  if(clearMask)
    gl().glClear(clearMask);
  clearMask = 0;
}

void MisakaFbo::doneRendering()
{

}

void MisakaFbo::bind() const
{
  gl().glBindFramebuffer(GL_FRAMEBUFFER, ID());
  gl().glClearColor(0,0,0,0);
}

void MisakaFbo::unbind(GLuint defaultFramebuffer)
{
  gl().glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
  gl().glClearColor(0,0,0,1);
}

void MisakaFbo::attach(MisakaTexture *texture, int texLayer, GLenum mode)
{
  attach(mode);
  if(texLayer != -1)
    texture->attachLayerToFbo(texLayer, mode);
  else
    texture->attachToFbo(mode);
}

void MisakaFbo::attach(MisakaRenderbuffer *renderbuffer, GLenum mode)
{
  attach(mode);
  renderbuffer->attachToFbo(mode);
}
