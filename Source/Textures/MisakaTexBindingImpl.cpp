#include "MisakaTexBindingImpl.hpp"
#include "MisakaTextureImpl.hpp"
#include "MisakaGpuBufImpl.hpp"
#include <Kawaii3D/KawaiiRoot.hpp>
#include <Kawaii3D/KawaiiGpuBuf.hpp>
#include <Kawaii3D/Textures/KawaiiTexture.hpp>

MisakaTexBindingImpl::MisakaTexBindingImpl(KawaiiTexBinding *model):
  KawaiiRendererImpl(model),
  model(model),
  bindingName(model? model->objectName() + "_handle": QString::null)
{
  if(model)
    {
      connect(model, &KawaiiTexBinding::targetChanged, this, &MisakaTexBindingImpl::onTargetChanged);
      connect(model, &QObject::objectNameChanged, this, &MisakaTexBindingImpl::onBindingNameChanged);
    } else
    deleteLater();
}

MisakaTexBindingImpl::~MisakaTexBindingImpl()
{
  unbind();
}

void MisakaTexBindingImpl::bind()
{
  if(auto owner = getOwner(); owner)
    {
      auto target = getTarget();
      uint64_t texHandle = target? target->getTextureHandle(): 0;
      owner->querrySetField(bindingName, texHandle);
    }
}

void MisakaTexBindingImpl::unbind()
{
  if(auto owner = getOwner(); owner)
    {
      uint64_t texHandle = 0;
      owner->querrySetField(bindingName, texHandle);
    }
}

void MisakaTexBindingImpl::onTargetChanged()
{
  disconnect(onTargetHandleChanged);
  auto target = getTarget();
  if(target)
    {
      onTargetHandleChanged = connect(target, &MisakaTextureImpl::textureHandleChanged, this, &MisakaTexBindingImpl::bind);
      bind();
    }
}

void MisakaTexBindingImpl::onBindingNameChanged(const QString &bindingName)
{
  QString _bindingName = bindingName + "_handle";
  if(this->bindingName != _bindingName)
    {
      unbind();
      this->bindingName = _bindingName;
      bind();
    }
}

MisakaTextureImpl *MisakaTexBindingImpl::getTarget() const
{
  if(model->getTarget())
    return static_cast<MisakaTextureImpl*>(model->getTarget()->getRendererImpl());
  else
    return nullptr;
}

MisakaGpuBufImpl *MisakaTexBindingImpl::getOwner() const
{
  if(model->getOwner())
    return static_cast<MisakaGpuBufImpl*>(model->getOwner()->getRendererImpl());
  else
    return nullptr;
}

void MisakaTexBindingImpl::initConnections()
{
  onTargetChanged();
}
