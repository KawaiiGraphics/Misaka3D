#ifndef MISAKAIMG_HPP
#define MISAKAIMG_HPP

#include "MisakaTexture.hpp"
#include <memory>
#include <QImage>

class MISAKA3D_SHARED_EXPORT MisakaImg : public MisakaTexture
{
  Q_OBJECT

public:
  MisakaImg(MisakaContext3D *ctx, const QImage *img);
  ~MisakaImg() = default;

  // MisakaTexture interface
private:
  void fill() override;



  //IMPLEMENT
private:
  const QImage *img;
  std::unique_ptr<QImage> convertedImg;

  const uchar* constBits() const;
};

#endif // MISAKAIMG_HPP
