#include "MisakaTextureFilter.hpp"

MisakaTextureFilter getMisakaTextureFilter(KawaiiTextureFilter value)
{
  switch(value)
    {
    case KawaiiTextureFilter::Linear:
      return MisakaTextureFilter::Linear;

    case KawaiiTextureFilter::LinearMipmapLinear:
      return MisakaTextureFilter::LinearMipmapLinear;

    case KawaiiTextureFilter::LinearMipmapNearest:
      return MisakaTextureFilter::LinearMipmapNearest;

    case KawaiiTextureFilter::Nearest:
      return MisakaTextureFilter::Nearest;

    case KawaiiTextureFilter::NearestMipmapLinear:
      return MisakaTextureFilter::NearestMipmapLinear;

    case KawaiiTextureFilter::NearestMipmapNearest:
      return MisakaTextureFilter::NearestMipmapNearest;
    }

  return MisakaTextureFilter::Nearest;
}

bool needsMipMap(MisakaTextureFilter filter)
{
  switch (filter)
    {
    case MisakaTextureFilter::LinearMipmapLinear:
    case MisakaTextureFilter::LinearMipmapNearest:
    case MisakaTextureFilter::NearestMipmapLinear:
    case MisakaTextureFilter::NearestMipmapNearest:
      return true;

    default:
      return false;
    }
}
