#include "MisakaTexture.hpp"
#include "OrphanGLTexture.hpp"
#include <glm/vec4.hpp>
#include <glm/vec3.hpp>

MisakaTexture::MisakaTexture(MisakaContext3D *ctx, const glm::uvec4 &sz, MisakaTextureType type):
  MisakaGLObject(ctx),
  currentSz(sz),
  type(type),
  handle(0)
{
}

void MisakaTexture::setSize(const glm::uvec4 &sz)
{
  if(glm::vec3(currentSz) == glm::vec3(sz))
    return;

  currentSz.x = sz.x;
  currentSz.y = sz.y;
  currentSz.z = sz.z;
  reinit();
}

void MisakaTexture::setMinFilter(MisakaTextureFilter value)
{
  if(minFilter != value)
    {
      bool needsRealloc = needsMipMap(minFilter) != needsMipMap(value);

      minFilter = value;
      if(needsRealloc)
        reinit();
      else
        update();
    }
}

void MisakaTexture::setMagFilter(MisakaTextureFilter value)
{
  if(magFilter != value)
    {
      bool needsRealloc = needsMipMap(magFilter) != needsMipMap(value);

      magFilter = value;
      if(needsRealloc)
        reinit();
      else
        update();
    }
}

void MisakaTexture::setWrapModeS(MisakaTextureWrapMode value)
{
  if(wrapModeS != value)
    {
      wrapModeS = value;
      update();
    }
}

void MisakaTexture::setWrapModeT(MisakaTextureWrapMode value)
{
  if(wrapModeT != value)
    {
      wrapModeT = value;
      update();
    }
}

void MisakaTexture::setWrapModeR(MisakaTextureWrapMode value)
{
  if(wrapModeR != value)
    {
      wrapModeR = value;
      update();
    }
}

MisakaTextureFilter MisakaTexture::getMinFilter() const
{
  return minFilter;
}

MisakaTextureFilter MisakaTexture::getMagFilter() const
{
  return magFilter;
}

MisakaTexture::~MisakaTexture()
{
}

void MisakaTexture::attachToFbo(GLenum mode) const
{
  gl().glFramebufferTexture(GL_FRAMEBUFFER, mode, ID(), 0);
}

void MisakaTexture::attachLayerToFbo(int layer, GLenum mode) const
{
  gl().glFramebufferTextureLayer(GL_FRAMEBUFFER, mode, ID(), 0, layer);
}

void MisakaTexture::initialize(GLuint &id)
{
  gl().glCreateTextures(type, 1, &id);
  setOrphanHandle(new OrphanGLTexture(id));
  if(needsMipMap(minFilter) || needsMipMap(magFilter))
    currentSz.w = 4;
  else
    currentSz.w = 1;

  gl().textureStorage(id, type, currentSz);
}

void MisakaTexture::updateNow()
{
  gl().glTextureParameteri(ID(), GL_TEXTURE_MIN_FILTER, static_cast<GLenum>(minFilter));
  gl().glTextureParameteri(ID(), GL_TEXTURE_MAG_FILTER, static_cast<GLenum>(magFilter));
  gl().glTextureParameteri(ID(), GL_TEXTURE_WRAP_S, static_cast<GLenum>(wrapModeS));
  gl().glTextureParameteri(ID(), GL_TEXTURE_WRAP_T, static_cast<GLenum>(wrapModeT));
  gl().glTextureParameteri(ID(), GL_TEXTURE_WRAP_R, static_cast<GLenum>(wrapModeR));

  gl().setTextureAnisotropy(ID());

  fill();

  if(needsMipMap(getMinFilter()) || needsMipMap(getMagFilter()))
    gl().glGenerateTextureMipmap(ID());

  if(!handle)
    {
      handle = gl().glGetTextureHandle(ID());
      if(gl().nvidiaShaders5())
        gl().glMakeTextureHandleResident(handle);
      emit handleChanged(handle);
    }
}
