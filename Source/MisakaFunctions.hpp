#ifndef MISAKAFUNCTIONS_HPP
#define MISAKAFUNCTIONS_HPP

#include <QOpenGLExtraFunctions>
#include <QRect>
#include "Shaders/MisakaShaderType.hpp"
#include "Textures/MisakaTextureType.hpp"
#include "MisakaGLBufferType.hpp"
#include "MisakaGLVariableType.hpp"
#include "Misaka3D_global.hpp"

using BaseGLFunc = QOpenGLExtraFunctions;

class MISAKA3D_SHARED_EXPORT MisakaFunctions : public BaseGLFunc
{  
public:
  MisakaFunctions();
  ~MisakaFunctions();

  struct DriverIssues
  {
    bool No_BindlessTexture: 1;
    bool No_DSA_Textures: 1;
    bool No_DSA_Buffers: 1;
    bool No_DSA_VAO: 1;
    bool No_DSA_FBO: 1;
    bool No_DSA_RBO: 1;
  };

  DriverIssues check() const;

  inline const QString& getShader5GlslExtension() const
  { return gpuShader5; }

  inline bool nvidiaShaders5() const
  { return nvShaders5; }

  inline void glBindBuffer(MisakaGLBufferType target, GLuint buffer)
  { return BaseGLFunc::glBindBuffer(static_cast<GLenum>(target), buffer); }

  inline void glBindBufferBase(MisakaGLBufferType target, GLuint index, GLuint buffer)
  { return BaseGLFunc::glBindBufferBase(static_cast<GLenum>(target), index, buffer); }

  inline void glBindBufferRange(MisakaGLBufferType target, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size)
  { return BaseGLFunc::glBindBufferRange(static_cast<GLenum>(target), index, buffer, offset, size); }

  inline void glBufferSubData(MisakaGLBufferType target, qopengl_GLintptr offset, qopengl_GLsizeiptr size, const void* data)
  { return BaseGLFunc::glBufferSubData(static_cast<GLenum>(target), offset, size, data); }

  using BaseGLFunc::glViewport;

  inline void glViewport(const QRect &r)
  { return glViewport(r.x(), r.y(), r.width(), r.height()); }

  inline void glVertexAttribPointer(GLuint index, GLint size, MisakaGLVariableType type, bool normalized, GLsizei stride, GLsizei offset)
  { return BaseGLFunc::glVertexAttribPointer(index, size, static_cast<GLenum>(type), normalized, stride, reinterpret_cast<void*>(offset)); }

  inline GLuint glCreateShader(MisakaShaderType type)
  { return BaseGLFunc::glCreateShader(static_cast<GLenum>(type)); }

  inline void glVertexAttribFormat(GLuint attribindex, GLint size, MisakaGLVariableType type, bool normalized, GLuint relativeoffset)
  { return BaseGLFunc::glVertexAttribFormat(attribindex, size, static_cast<GLenum>(type), normalized, relativeoffset); }

  void glShaderSource(GLuint shader, const QString &source);

  inline void glBindTexture(MisakaTextureType target, GLuint texture)
  { return BaseGLFunc::glBindTexture(static_cast<GLenum>(target), texture); }

  GLboolean glIsImageHandleResident(GLuint64 handle);
  GLboolean glIsTextureHandleResident(GLuint64 handle);
  void glProgramUniformHandleui64v(GLuint program, GLint location, GLsizei count, const GLuint64 *values);
  void glProgramUniformHandleui64(GLuint program, GLint location, GLuint64 value);
  void glUniformHandleui64v(GLint location, GLsizei count, const GLuint64 *value);
  void glUniformHandleui64(GLint location, GLuint64 value);
  void glMakeImageHandleNonResident(GLuint64 handle);
  void glMakeImageHandleResident(GLuint64 handle, GLenum access);
  GLuint64 glGetImageHandle(GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum format);
  void glMakeTextureHandleNonResident(GLuint64 handle);
  void glMakeTextureHandleResident(GLuint64 handle);
  GLuint64 glGetTextureSamplerHandle(GLuint texture, GLuint sampler);
  GLuint64 glGetTextureHandle(GLuint texture);

  void glCreateTextures(MisakaTextureType target, GLsizei n, GLuint *textures);
  void glTextureStorage1D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width);
  void glTextureStorage2D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height);
  void glTextureStorage3D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth);
  void glTextureSubImage1D(GLuint texture, GLint level, GLint xoffset, GLsizei width, GLenum format, MisakaGLVariableType type, const void *pixels);
  void glTextureSubImage2D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, MisakaGLVariableType type, const void *pixels);
  void glTextureSubImage3D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, MisakaGLVariableType type, const void *pixels);

  void glCreateBuffers(GLsizei n, GLuint *buffers);
  void glNamedBufferStorage(GLuint buffer, GLsizei size, const void *data, GLbitfield flags);
  void glNamedBufferSubData(GLuint buffer, GLintptr offset, GLsizei size, const void *data);

  void glCreateVertexArrays(GLsizei n, GLuint *arrays);
  void glVertexArrayVertexBuffer(GLuint vaobj, GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride);
  void glEnableVertexArrayAttrib(GLuint vaobj, GLuint index);
  void glDisableVertexArrayAttrib(GLuint vaobj, GLuint index);
  void glVertexArrayAttribFormat(GLuint vaobj, GLuint attribindex, GLint size, MisakaGLVariableType type, GLboolean normalized, GLuint relativeoffset);
  void glVertexArrayAttribBinding(GLuint vaobj, GLuint attribindex, GLuint bindingindex);
  void glVertexArrayElementBuffer(GLuint vaobj, GLuint buffer);

  void glCreateFramebuffers(GLsizei n, GLuint *ids);
  void glNamedFramebufferTexture(GLuint framebuffer, GLenum attachment, GLuint texture, GLint level);

  void glCreateRenderbuffers(GLsizei n, GLuint *renderbuffers);
  void glNamedRenderbufferStorage(GLuint renderbuffer, GLenum internalformat, GLsizei width, GLsizei height); 
  void glNamedFramebufferRenderbuffer(GLuint framebuffer, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer);

  void glTextureParameteri(GLuint texture, GLenum pname, GLint param);
  void glTextureParameterf(GLuint texture, GLenum pname, GLfloat param);
  void glGenerateTextureMipmap(GLuint texture);

  void setTextureAnisotropy(GLuint texture);
  void setTextureAnisotropy(GLuint texture, GLfloat factor);

  void textureStorage(GLuint texture, MisakaTextureType type, const glm::uvec4 &sz);

  void initialize();



  //IMPLEMENT
private:
  QString gpuShader5;

  //ARB_bindless_texture
  GLboolean (*isImageHandleResident) (GLuint64 handle);
  GLboolean (*isTextureHandleResident) (GLuint64 handle);
  void (*programUniformHandleui64v) (GLuint program, GLint location, GLsizei count, const GLuint64 *values);
  void (*programUniformHandleui64) (GLuint program, GLint location, GLuint64 value);
  void (*uniformHandleui64v) (GLint location, GLsizei count, const GLuint64 *value);
  void (*uniformHandleui64) (GLint location, GLuint64 value);
  void (*makeImageHandleNonResident) (GLuint64 handle);
  void (*makeImageHandleResident) (GLuint64 handle, GLenum access);
  GLuint64 (*getImageHandle) (GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum format);
  void (*makeTextureHandleNonResident) (GLuint64 handle);
  void (*makeTextureHandleResident) (GLuint64 handle);
  GLuint64 (*getTextureSamplerHandle) (GLuint texture, GLuint sampler);
  GLuint64 (*getTextureHandle) (GLuint texture);

  //4.5 Core
  void (*createTextures) (GLenum target, GLsizei n, GLuint *textures);
  void (*textureStorage1D)(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width);
  void (*textureStorage2D)(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height);
  void (*textureStorage3D)(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth);
  void (*textureSubImage1D)(GLuint texture, GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type, const void *pixels);
  void (*textureSubImage2D)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels);
  void (*textureSubImage3D)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *pixels);

  void (*createBuffers) (GLsizei n, GLuint *buffers);
  void (*namedBufferStorage) (GLuint buffer, GLsizei size, const void *data, GLbitfield flags);
  void (*namedBufferSubData) (GLuint buffer, GLintptr offset, GLsizei size, const void *data);

  void (*createVertexArrays)(GLsizei n, GLuint *arrays);
  void (*vertexArrayVertexBuffer)(GLuint vaobj, GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride);
  void (*enableVertexArrayAttrib)(GLuint vaobj, GLuint index);
  void (*disableVertexArrayAttrib)(GLuint vaobj, GLuint index);
  void (*vertexArrayAttribFormat)(GLuint vaobj, GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset);
  void (*vertexArrayAttribBinding)(GLuint vaobj, GLuint attribindex, GLuint bindingindex);
  void (*vertexArrayElementBuffer)(GLuint vaobj, GLuint buffer);

  void (*createFramebuffers)(GLsizei n, GLuint *ids);
  void (*namedFramebufferTexture)(GLuint framebuffer, GLenum attachment, GLuint texture, GLint level);

  void (*createRenderbuffers)(GLsizei n, GLuint *renderbuffers);
  void (*namedRenderbufferStorage)(GLuint renderbuffer, GLenum internalformat, GLsizei width, GLsizei height);
  void (*namedFramebufferRenderbuffer)(GLuint framebuffer, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer);

  void (*textureParameteri)(GLuint texture, GLenum pname, GLint param);
  void (*textureParameterf)(GLuint texture, GLenum pname, GLfloat param);
  void (*generateTextureMipmap)(GLuint texture);

  GLfloat maxAnisotropy;
  bool nvShaders5;
};

#endif // MISAKAFUNCTIONS_HPP
