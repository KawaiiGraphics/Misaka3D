#ifndef MISAKADIRTYGLOBJECTS_HPP
#define MISAKADIRTYGLOBJECTS_HPP

#include "Misaka3D_global.hpp"
#include <QLinkedList>

class MisakaGLObject;

class MISAKA3D_SHARED_EXPORT MisakaDirtyGLObjects
{
  QLinkedList<MisakaGLObject*> lst;

public:
  MisakaDirtyGLObjects() = default;
  ~MisakaDirtyGLObjects() = default;

  void check();

  void update(MisakaGLObject *obj);
  void remove(MisakaGLObject *obj);
};

#endif // MISAKADIRTYGLOBJECTS_HPP
