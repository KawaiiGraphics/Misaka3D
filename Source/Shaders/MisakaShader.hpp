#ifndef MISAKASHADER_HPP
#define MISAKASHADER_HPP

#include "../MisakaGLObject.hpp"
#include "MisakaShaderType.hpp"

class MisakaShader: public MisakaGLObject
{
  friend class MisakaShaderImpl;
  Q_OBJECT

public:
  template<typename... ArgsT>
  MisakaShader(MisakaShaderType type, ArgsT&&... args):
    MisakaGLObject(std::forward<ArgsT>(args)...), type(type)
  { }

  ~MisakaShader();

  void setGlsl(const QString &value);

  // MisakaGLObject interface    
protected:
  void initialize(GLuint &id) override;
  void updateNow() override;

signals:
  void shaderRecompiled(MisakaShader *shader);



  //IMPLEMENT
private:
  QString glsl;
  MisakaShaderType type;
};

#endif // MISAKASHADER_HPP
