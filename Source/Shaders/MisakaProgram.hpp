#ifndef MISAKAPROGRAM_HPP
#define MISAKAPROGRAM_HPP

#include "../MisakaGLObject.hpp"
#include <QSet>

class MISAKA3D_SHARED_EXPORT MisakaProgram: public MisakaGLObject
{
  Q_OBJECT
public:
  MisakaProgram(MisakaContext3D *ctx);
  ~MisakaProgram();

  void attachShader(GLuint shader);
  void detachShader(GLuint shader);
  void use() const;

  // MisakaGLObject interface
protected:
  void initialize(GLuint &id) override;
  void updateNow() override;



  //IMPLEMENT
private:
  QSet<GLuint> shaders;
  QLinkedList<GLuint> attaching_shaders;
  QLinkedList<GLuint> detaching_shaders;
};

#endif // MISAKAPROGRAM_HPP
