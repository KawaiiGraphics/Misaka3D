#include "OrphanGLTexture.hpp"
#include "../MisakaFunctions.hpp"

OrphanGLTexture::OrphanGLTexture(GLuint id):
  id(id)
{ }

void OrphanGLTexture::destroy(MisakaFunctions &gl)
{
  gl.glDeleteTextures(1, &id);
}
