#include <QDebug>
#include "MisakaShader.hpp"
#include "OrphanShader.hpp"
#include <Kawaii3D/KawaiiConfig.hpp>

MisakaShader::~MisakaShader()
{
}

void MisakaShader::initialize(GLuint &id)
{
  id = gl().glCreateShader(type);
  setOrphanHandle(new OrphanShader(id));
}

void MisakaShader::updateNow()
{
  gl().glShaderSource(ID(), glsl);
  gl().glCompileShader(ID());
  if(KawaiiConfig::getInstance().getDebugLevel() > 0)
    {
      int infoLogLength;
      gl().glGetShaderiv(ID(), GL_INFO_LOG_LENGTH, &infoLogLength);
      if ( infoLogLength > 0 )
        {
          GLint success;
          gl().glGetShaderiv(ID(), GL_COMPILE_STATUS, &success);
          if(!success)
            qCritical() << objectName() << " (MisakaShader): compilation failed!";

          std::vector<char> msg(infoLogLength+1),
              src(glsl.length());

          gl().glGetShaderInfoLog(ID(), infoLogLength, nullptr, &msg[0]);
          gl().glGetShaderSource(ID(), glsl.length(), nullptr, &src[0]);

          QString srcStr(&src[0]),
              msgStr(&msg[0]);

          auto leftTabs = [](QString &str)
          {
            str.push_front('\t');
            str.replace('\n', "\n\t");
          };

          leftTabs(srcStr);
          leftTabs(msgStr);

          qCritical() << "Problematic shader code:";
          qCritical().noquote() << srcStr;
          qCritical() << "::::::\r\nShader compile log:";
          qCritical().noquote() << msgStr;
          qCritical().quote();
        }
    }
  emit shaderRecompiled(this);
}

void MisakaShader::setGlsl(const QString &value)
{
  glsl = value;
  update();
}
