#include "MisakaBufBindingImpl.hpp"
#include <Kawaii3D/KawaiiRoot.hpp>
#include "MisakaRootImpl.hpp"

MisakaBufBindingImpl::MisakaBufBindingImpl(KawaiiBufBinding *model):
  KawaiiRendererImpl(model),
  buf(nullptr),
  model(model),
  root(nullptr)
{
  if(model)
    {
      connect(model, &KawaiiBufBinding::bufferChanged, this, &MisakaBufBindingImpl::updateTarget);
      connect(model, &KawaiiBufBinding::bindingPointChanged, this, &MisakaBufBindingImpl::updateView);
      connect(model, &KawaiiBufBinding::targetChanged, this, &MisakaBufBindingImpl::updateView);
      connect(model, &KawaiiBufBinding::parentUpdated, this, &MisakaBufBindingImpl::onDataParentChanged);

      onDataParentChanged();
    } else
    deleteLater();
}

MisakaBufBindingImpl::~MisakaBufBindingImpl()
{
  if(view)
    {
      view->deleteLater();
      view.clear();
    }
}

void MisakaBufBindingImpl::onDataParentChanged()
{
  auto _root = MisakaRootImpl::findRootImpl(this);
  if(_root != root)
    {
      root = _root;
      createView();
    }
}

void MisakaBufBindingImpl::allocView()
{
  if(root && buf)
    {
      view = new MisakaGLBufBinding(model->getBindingPoint(), buf, root->getContext(), root->getContext());
      view->setTarget(model->getTarget());
    }
}

void MisakaBufBindingImpl::createView()
{
  if(view)
    {
      view->deleteLater();
      view.clear();
    }
  allocView();
}

void MisakaBufBindingImpl::updateTarget()
{
  if(buf)
    disconnect(buf->getBuffer(), &MisakaGLBuffer::initialized, this, &MisakaBufBindingImpl::bindImmediately);

  buf = static_cast<MisakaGpuBufImpl*>(model->getBuf()->getRendererImpl());
  connect(buf->getBuffer(), &MisakaGLBuffer::initialized, this, &MisakaBufBindingImpl::bindImmediately);
  updateView();
}

void MisakaBufBindingImpl::updateView()
{
  if(view)
    {
      view->bindingPoint = model->getBindingPoint();
      view->setTarget(model->getTarget());
      view->buffer = buf;
      view->reinit();
    } else
    allocView();
}

void MisakaBufBindingImpl::bindImmediately()
{
  GLuint fakeId = 0;
  view->initialize(fakeId);
}

void MisakaBufBindingImpl::initConnections()
{
  updateTarget();
}
