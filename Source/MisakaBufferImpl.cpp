#include "MisakaBufferImpl.hpp"

MisakaBufferImpl::MisakaBufferImpl(MisakaGLBuffer *glBuf):
  KawaiiRendererImpl(nullptr),
  glBuf(glBuf)
{ }

void MisakaBufferImpl::setBuffer(MisakaGLBuffer *buf)
{
  glBuf = buf;
}

void MisakaBufferImpl::deleteBuffer()
{
  if(glBuf)
    {
      glBuf->deleteLater();
      glBuf.clear();
    }
}
