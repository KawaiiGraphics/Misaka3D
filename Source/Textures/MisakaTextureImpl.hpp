#ifndef MISAKATEXTUREIMPL_HPP
#define MISAKATEXTUREIMPL_HPP

#include <QPointer>
#include "../Misaka3D_global.hpp"
#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include <Kawaii3D/Textures/KawaiiFramebuffer.hpp>
#include <Kawaii3D/Textures/KawaiiTextureFilter.hpp>
#include <Kawaii3D/Textures/KawaiiTextureWrapMode.hpp>

class KawaiiTexture;

class MisakaTexture;
class MisakaFbo;

class MISAKA3D_SHARED_EXPORT MisakaTextureImpl: public KawaiiRendererImpl
{
  Q_OBJECT
public:
  explicit MisakaTextureImpl(KawaiiTexture *model);
  ~MisakaTextureImpl();

  uint64_t getTextureHandle() const;

  void attachToFbo(MisakaFbo *fbo, KawaiiFramebuffer::AttachmentMode mode, int texLayer) const;

protected:
  MisakaTexture* getView() const;
  void setView(MisakaTexture *view);

signals:
  void textureHandleChanged(uint64_t handle);



  //IMPLEMENT
private:
  KawaiiTexture *model;
  QPointer<MisakaTexture> view;

  void onModelMinFilterChanged(KawaiiTextureFilter filter);
  void onModelMagFilterChanged(KawaiiTextureFilter filter);

  void onModelWrapModeSChanged(KawaiiTextureWrapMode mode);
  void onModelWrapModeTChanged(KawaiiTextureWrapMode mode);
  void onModelWrapModeRChanged(KawaiiTextureWrapMode mode);
};

#endif // MISAKATEXTUREIMPL_HPP
