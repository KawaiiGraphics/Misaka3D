#ifndef MISAKAMATERIALIMPL_HPP
#define MISAKAMATERIALIMPL_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include "MisakaGpuBufImpl.hpp"
#include <qopengl.h>

class KawaiiMaterial;
class MisakaRootImpl;

class MISAKA3D_SHARED_EXPORT MisakaMaterialImpl: public KawaiiRendererImpl
{
  Q_OBJECT
public:
  MisakaMaterialImpl(KawaiiMaterial *model);
  ~MisakaMaterialImpl();

  void bind() const;



  //IMPLEMENT
private:
  KawaiiMaterial *model;
  MisakaGpuBufImpl *ubo;
};

#endif // MISAKAMATERIALIMPL_HPP
