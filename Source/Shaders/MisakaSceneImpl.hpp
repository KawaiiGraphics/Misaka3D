#ifndef MISAKASCENEIMPL_HPP
#define MISAKASCENEIMPL_HPP

#include <Kawaii3D/Shaders/KawaiiScene.hpp>
#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include "../Misaka3D_global.hpp"

class MisakaGLBuffer;
class MISAKA3D_SHARED_EXPORT MisakaSceneImpl: public KawaiiRendererImpl
{
  Q_OBJECT
public:
  MisakaSceneImpl(KawaiiScene *model);
  ~MisakaSceneImpl();

  void draw(MisakaGLBuffer *cameraUBO, MisakaGLBuffer *surfaceUBO);



  //IMPLEMENT
private:
  KawaiiScene *model;
};

#endif // MISAKASCENEIMPL_HPP
