#ifndef MISAKAGLOBJECT_HPP
#define MISAKAGLOBJECT_HPP

#include <QObject>
#include "MisakaContext3D.hpp"
#include "MisakaDirtyGLObjects.hpp"
#include "OrphanGLData.hpp"

class MISAKA3D_SHARED_EXPORT MisakaGLObject : public QObject
{
  Q_OBJECT
  friend class MisakaDirtyGLObjects;
private:
  using QObject::setParent;

public:
  explicit MisakaGLObject(MisakaContext3D *ctx);
  MisakaGLObject(MisakaContext3D *ctx, QObject *parent);
  MisakaGLObject(MisakaGLObject *glParent, QObject *parent = nullptr);

  virtual ~MisakaGLObject();

  void update();
  void destroy();

  void checkDirtyChildren();

  inline bool isValid() const { return exists; }

  inline auto* getCtx() const { return ctx; }

signals:
  void initialized();



  //IMPLEMENT
private:
  MisakaDirtyGLObjects dirtyChildren;
  QVector<MisakaGLObject*> glChildren;
  MisakaGLObject *glParent;
  MisakaContext3D *ctx;
  OrphanGLData *orphanHandle;
  GLuint id;
  bool exists;

  virtual void initialize(GLuint &id) = 0;
  virtual void updateNow() = 0;

protected:
  void check();

  inline auto& gl() const { return ctx->gl; }

  void setOrphanHandle(OrphanGLData *handle);
  OrphanGLData *getOrphanHandle() const;

  inline GLuint ID() const { return id; }

  inline bool isExists() const { return exists; }

  void reinit();
};

#endif // MISAKAGLOBJECT_HPP
