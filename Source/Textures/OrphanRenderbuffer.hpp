#ifndef ORPHANRENDERBUFFER_HPP
#define ORPHANRENDERBUFFER_HPP

#include "../OrphanGLData.hpp"
#include <qopengl.h>

class OrphanRenderbuffer: public OrphanGLData
{
  GLuint id;
public:
  OrphanRenderbuffer(GLuint id);

  // OrphanGLData interface
  void destroy(MisakaFunctions &gl) override;
};

#endif // ORPHANRENDERBUFFER_HPP
