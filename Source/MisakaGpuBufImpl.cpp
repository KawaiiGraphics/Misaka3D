#include "MisakaGpuBufImpl.hpp"
#include "MisakaRootImpl.hpp"
#include "MisakaGLUniformBufferMap.hpp"
#include <Kawaii3D/KawaiiGpuBuf.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>

MisakaGpuBufImpl::MisakaGpuBufImpl(KawaiiGpuBuf *model):
  MisakaBufferImpl(nullptr, model),
  advBuf(nullptr),
  model(model)
{
  if(model)
    {
      connect(model, &KawaiiGpuBuf::parentUpdated, this, &MisakaGpuBufImpl::onDataParentChanged);
      onDataParentChanged();
    } else
    deleteLater();
}

MisakaGpuBufImpl::~MisakaGpuBufImpl()
{
  deleteView();
  root = nullptr;
}

void MisakaGpuBufImpl::querrySetField(const QString &fieldName, const void *memPtr, size_t memSize)
{
  if(!root)
    return;

  if(!advBuf)
    advBuf = new MisakaGLUniformBufferMap(root->getContext());

  advBuf->querrySetUboField(fieldName, memPtr, memSize);
}

void MisakaGpuBufImpl::bind(GLuint blockIndex, MisakaGLBufferType target) const
{
  if(getBuffer())
    getBuffer()->bindToBlock(blockIndex, target);

  if(advBuf)
    advBuf->bindToBlock(blockIndex);
}

void MisakaGpuBufImpl::unbind(GLuint blockIndex, MisakaGLBufferType target) const
{
  if(getBuffer())
    getBuffer()->unbindBlock(blockIndex, target);

  if(advBuf)
    advBuf->unbindBlock(blockIndex);
}

KawaiiGpuBuf *MisakaGpuBufImpl::getModel() const
{
  return model;
}

void MisakaGpuBufImpl::createBuffer()
{
  setBuffer(new MisakaGLBuffer(MisakaGLBufferType::UBO, root->getContext()));

  connect(model, &KawaiiGpuBuf::dataChanged, getBuffer(), static_cast<void(MisakaGLBuffer::*)(size_t, size_t)>(&MisakaGLBuffer::updateData));
  connect(model, &KawaiiGpuBuf::relocated, getBuffer(), &MisakaGLBuffer::setData);
}

void MisakaGpuBufImpl::deleteView()
{
  deleteBuffer();

  if(advBuf)
    {
      advBuf->deleteLater();
      advBuf.clear();
    }
}

void MisakaGpuBufImpl::setupBuffer()
{
  if(!getBuffer())
    createBuffer();

  getBuffer()->setData(model->getData(), model->getDataSize());
}

void MisakaGpuBufImpl::onDataParentChanged()
{
  auto _root = MisakaRootImpl::findRootImpl(this);
  if(_root != root)
    {
      if(root)
        deleteView();

      root = _root;
      if(root)
        setupBuffer();
    }
}
