#include "MisakaTextureType.hpp"
#include "MisakaFunctions.hpp"

MisakaTextureType getMisakaTextureType(KawaiiTextureType type)
{
  switch (type) {
    case KawaiiTextureType::Texture1D:
      return MisakaTextureType::Texture1D;

    case KawaiiTextureType::Texture2D:
      return MisakaTextureType::Texture2D;

    case KawaiiTextureType::Texture3D:
      return MisakaTextureType::Texture3D;

    case KawaiiTextureType::Texture1D_Array:
      return MisakaTextureType::Texture1D_Array;

    case KawaiiTextureType::Texture2D_Array:
      return MisakaTextureType::Texture2D_Array;

    case KawaiiTextureType::TextureRectangle:
      return MisakaTextureType::TextureRectangle;

    case KawaiiTextureType::TextureCube:
      return MisakaTextureType::TextureCube;

    case KawaiiTextureType::TextureCube_Array:
      return MisakaTextureType::TextureCube_Array;

    case KawaiiTextureType::TextureBuffer:
      return MisakaTextureType::TextureBuffer;

    case KawaiiTextureType::Texture2D_Multisample:
      return MisakaTextureType::Texture2D_Multisample;

    case KawaiiTextureType::Texture2D_Multisample_Array:
      return MisakaTextureType::Texture2D_Multisample_Array;
    }
  return MisakaTextureType::Invalid;
}
