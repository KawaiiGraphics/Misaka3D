#ifndef MISAKAGLVARIABLETYPE_HPP
#define MISAKAGLVARIABLETYPE_HPP

#include <qopengl.h>

enum class MisakaGLVariableType: GLenum
{
  Byte = GL_BYTE,
  UByte = GL_UNSIGNED_BYTE,

  Short = GL_SHORT,
  UShort = GL_UNSIGNED_SHORT,

  Int = GL_INT,
  UInt = GL_UNSIGNED_INT,

  HalfFloat = GL_HALF_FLOAT,
  Float = GL_FLOAT,
  Double = GL_DOUBLE,
  Fixed = GL_FIXED
};

#endif // MISAKAGLVARIABLETYPE_HPP
