#ifndef MISAKAMESHIMPL_HPP
#define MISAKAMESHIMPL_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include "../Misaka3D_global.hpp"
#include "../MisakaBufferImpl.hpp"

class MisakaMeshInstanceImpl;
class MisakaRootImpl;
class MisakaVao3D;

class KawaiiMesh3D;
class KawaiiTriangle3D;
class KawaiiPoint3D;

class MISAKA3D_SHARED_EXPORT MisakaMeshImpl : public KawaiiRendererImpl
{
  Q_OBJECT
  friend class MisakaMeshInstanceImpl; //uses void addChildInstance(MisakaMeshInstanceImpl *inst);

public:
  explicit MisakaMeshImpl(KawaiiMesh3D *model);
  ~MisakaMeshImpl();



  //IMPLEMENT
private:
  MisakaBufferImpl baseAttrCtrl;

  QPointer<MisakaVao3D> vao;
  KawaiiMesh3D *model;
  MisakaRootImpl *root;

  void addChildInstance(MisakaMeshInstanceImpl *inst);

  void createView();

  void vertexAdded();
  void vertexRemoved();
  void vertexUpdated(int index);

  void trianglesRelocated();
  void indexUpdated(int i);

  void onDataParentChanges();
};

#endif // MISAKAMESHIMPL_HPP
