#include "MisakaRootImpl.hpp"
#include "MisakaContext3D.hpp"
#include <Kawaii3D/KawaiiRoot.hpp>
#include <QChildEvent>
#include <QLinkedList>

MisakaRootImpl::MisakaRootImpl(KawaiiRoot *model):
  KawaiiRendererImpl(model),
  ctx(model? new MisakaContext3D(this): nullptr)
{
  if(ctx)
    ctx->setObjectName("OpenGL");
}

MisakaRootImpl::~MisakaRootImpl()
{
  if(ctx)
    delete ctx;
}

MisakaRootImpl *MisakaRootImpl::findRootImpl(KawaiiRendererImpl *rnd)
{
  auto root_data = KawaiiRoot::getRoot(rnd->getData());
  if(!root_data)
    return nullptr;

  return static_cast<MisakaRootImpl*>(root_data->getRendererImpl());
}
